package com.mfs.client.mascom.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.mascom.dao.SystemConfigDao;
import com.mfs.client.mascom.dto.ResponseStatus;
import com.mfs.client.mascom.exception.DaoException;
import com.mfs.client.mascom.models.MascomSystemConfig;

@EnableTransactionManagement
@Repository("SystemConfigDao")
public class SystemConfigDaoImpl implements SystemConfigDao {

	
	
	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(SystemConfigDaoImpl.class);

	@Transactional
	public Map<String, String> getConfigDetailsMap() throws DaoException {

		LOGGER.info("Inside  getConfigDetailsMap");
		Map<String, String> systemConfigMap = new HashMap<String, String>();
		Session session = sessionFactory.getCurrentSession();
		String hql = "From MascomSystemConfig";
		Query query = session.createQuery(hql);

		try { 
			List<MascomSystemConfig> systemConfig = query.list(); 

			if (systemConfig != null && !systemConfig.isEmpty()) {
				systemConfigMap = new HashMap<String, String>();
				for (MascomSystemConfig systemConfiguration : systemConfig) {
					systemConfigMap.put(systemConfiguration.getConfigKey(),
							systemConfiguration.getConfigValue());
				}
			}
		} catch (Exception e) {
			LOGGER.error("==>Exception thrown in getConfigDetailsMap ");
			ResponseStatus status = new ResponseStatus();

		//status.setStatusCode(MFSAMKenyaConstants.AMER207);
		//status.setStatusMessage(responseCodes.getProperty(MFSAMKenyaConstants.AMER207));
			throw new DaoException(status);
		}
		return systemConfigMap;
	}

}
