package com.mfs.client.mascom.service;

import com.mfs.client.mascom.dto.ViewAccountTypeRequestDto;

import com.mfs.client.mascom.dto.ViewAccountTypeResponseDto;

public interface ViewAccountTypeService {
		
	ViewAccountTypeResponseDto viewAccountType(ViewAccountTypeRequestDto request);
	
}
