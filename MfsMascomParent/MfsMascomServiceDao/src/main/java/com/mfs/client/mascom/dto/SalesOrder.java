package com.mfs.client.mascom.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import javax.xml.bind.annotation.XmlAttribute;

@XmlRootElement(name = "Function")
@XmlType(propOrder = { "mfsTransactionId", "salesOrderTransactionId" })
public class SalesOrder {
	@XmlAttribute(name="name")
	private String name="GetSalesOrderDetails";
	private String mfsTransactionId;
	private String salesOrderTransactionId;

	@XmlElement(name = "param1")
	public String getMfsTransactionId() {
		return mfsTransactionId;
	}
	
	public void setMfsTransactionId(String mfsTransactionId) {
		this.mfsTransactionId = mfsTransactionId;
	}
		
	@XmlElement(name = "param2")
	public String getSalesOrderTransactionId() {
		return salesOrderTransactionId;
	}
	
	public void setSalesOrderTransactionId(String salesOrderTransactionId) {
		this.salesOrderTransactionId = salesOrderTransactionId;
	}

	@Override
	public String toString() {
		return "SalesOrder [name=" + name + ", mfsTransactionId=" + mfsTransactionId + ", salesOrderTransactionId="
				+ salesOrderTransactionId + "]";
	}
	
}
