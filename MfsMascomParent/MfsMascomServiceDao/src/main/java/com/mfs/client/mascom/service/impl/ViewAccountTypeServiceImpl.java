package com.mfs.client.mascom.service.impl;

import java.util.Date;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.mfs.client.mascom.dao.SystemConfigDao;
import com.mfs.client.mascom.dao.TransactionDao;
import com.mfs.client.mascom.dto.GetSalesOrderResponseDto;
import com.mfs.client.mascom.dto.PaymentResponseDto;
import com.mfs.client.mascom.dto.ViewAccountType;
import com.mfs.client.mascom.dto.ViewAccountTypeRequestDto;
import com.mfs.client.mascom.dto.ViewAccountTypeResponseDto;
import com.mfs.client.mascom.exception.DaoException;
import com.mfs.client.mascom.models.MascomAuthorizationData;
import com.mfs.client.mascom.models.MascomViewAccount;
import com.mfs.client.mascom.service.ViewAccountTypeService;
import com.mfs.client.mascom.util.CommonConstant;
import com.mfs.client.mascom.util.HttpsConnectionRequest;
import com.mfs.client.mascom.util.HttpsConnectionResponse;
import com.mfs.client.mascom.util.MascomCodes;
import com.mfs.client.mascom.util.XmlParsingService;

/*
* @author Arnav Gharote
* ViewAccountTypeService.java
* 27-03-2020
*
* purpose: this class is used for ViewAccountTypeService
*
* Methods
* 1. viewAccountType
* 2. logResponse
*
*/
@Service("ViewAccountTypeService")
public class ViewAccountTypeServiceImpl implements ViewAccountTypeService {

	@Autowired
	SystemConfigDao systemConfigDao;

	@Autowired
	TransactionDao transactionDao;

	private static final Logger LOGGER = Logger.getLogger(ViewAccountTypeServiceImpl.class);

	public ViewAccountTypeResponseDto viewAccountType(ViewAccountTypeRequestDto request) {

		Map<String, String> headerMap = new HashMap<String, String>();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;
		ViewAccountTypeResponseDto response = null;
		Map<String, String> configMap = null;
		XmlParsingService parse = new XmlParsingService();
		String serviceResponse = null;

		try {
			// get last generated Token ID
			MascomAuthorizationData accessToken = transactionDao.getLastRecordByAccessToken();
			if (accessToken == null) {
				response = new ViewAccountTypeResponseDto();
				response.setCode(MascomCodes.ER214.getCode());
				response.setMessage(MascomCodes.ER214.getMessage());
			} else {
				// get System Config Details from DB
				configMap = systemConfigDao.getConfigDetailsMap();
				if (configMap == null) {
					throw new Exception("No config Details Found");
				}
				// Set View AccountType request
				request.setUserName(configMap.get(CommonConstant.USERNAME));
				request.setPassword(configMap.get(CommonConstant.PASSWORD));
				request.setTerminalType(configMap.get(CommonConstant.TERMINAL_TYPE));
				LOGGER.info("ViewAccountTypeServiceImpl in view account type service function " + request);

				// Converting object to xml
				String xml = parse.objectToXml(request);
				LOGGER.info("ViewAccountTypeServiceImpl in view account type xml request " + xml);

				// Set Header
				headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER + accessToken.getAccessToken());
				headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);

				// set service URL
				/*
				 * connectionRequest.setHttpmethodName(CommonConstant.HTTP_METHOD);
				 * connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)
				 * ));
				 * connectionRequest.setServiceUrl(configMap.get(CommonConstant.MASCOM_URL)+(
				 * configMap.get(CommonConstant.VIEW_ACCOUNT_TYPE_URL)));
				 * connectionRequest.setHeaders(headerMap);
				 */

				// Dummy response
				String responseString = "<?xml version=\"1.0\"?>\n" + "<TCSReply>\n" + "<Result>0</Result>\n"
						+ "<Message>View account type successful. Reference: 6789</Message>\n" + "<param1>0123</param1>\n"
						+ "<param2>0456</param2>\n" + "<param3>ArnavG</param3>\n" + "<param4>AAA</param4>\n"
						+ "<param5>Layer0888</param5>\n" + "<param6>Group0888</param6>\n" + "<param7>A</param7>\n"
						+ "<param8>POSAdmin111</param8>\n" + "</TCSReply>";
				LOGGER.info("ViewAccountTypeServiceImpl in ViewAccountTypeService function for XML response "
						+ responseString);

				// Call to the thMascom
				/*
				 * HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
				 * httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(xml,
				 * connectionRequest); String responseString= httpsConResult.getResponseData();
				 * LOGGER.
				 * info("Details ViewAccountTypeServiceImpl for ViewAccountTypeService function XML Response"
				 * + responseString);
				 */

				response = (ViewAccountTypeResponseDto) parse.xmlToObjectResponse(new ViewAccountTypeResponseDto(),
						responseString);
				LOGGER.info("ViewAccountTypeServiceImpl in ViewAccountTypeService  function " + response);

				if (response != null) {
					logResponse(request, response);
				} else {
					response = new ViewAccountTypeResponseDto();
					response.setCode(MascomCodes.ER201.getCode());
					response.setMessage(MascomCodes.ER201.getMessage());
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in ViewAccountTypeServiceImpl" + de);
			response = new ViewAccountTypeResponseDto();
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>DaoException in ViewAccountTypeServiceImpl" + e);
			response = new ViewAccountTypeResponseDto();
			response.setCode(MascomCodes.ER201.getCode());
			response.setMessage(MascomCodes.ER201.getMessage());
		}
		return response;
	}

	// Log response for view account type service
	private void logResponse(ViewAccountTypeRequestDto request, ViewAccountTypeResponseDto response)
			throws DaoException {

		MascomViewAccount mascomViewAccount = transactionDao
				.getByMsisdnOrAccountId(request.getFunction().getAccountMsisdn(), request.getFunction().getAccountId());

		if (mascomViewAccount == null) {

			MascomViewAccount viewAccount = new MascomViewAccount();

			viewAccount.setAccountMsisdn(request.getFunction().getAccountMsisdn());
			viewAccount.setAccountId(request.getFunction().getAccountId());
			viewAccount.setAccountName(response.getAccountName());
			viewAccount.setAccountCustomerType(response.getCustomerType());
			viewAccount.setAccountLayer(response.getAccountLayer());
			viewAccount.setAccountGroup(response.getGroup());
			viewAccount.setAccountStatus(response.getStatus());
			viewAccount.setTerminalUsernameMsisdn(response.getTerminalUserName());
			viewAccount.setDateLogged(new Date());
			transactionDao.save(viewAccount);

		} else {

			mascomViewAccount.setAccountName(response.getAccountName());
			mascomViewAccount.setAccountCustomerType(response.getCustomerType());
			mascomViewAccount.setAccountLayer(response.getAccountLayer());
			mascomViewAccount.setAccountGroup(response.getGroup());
			mascomViewAccount.setAccountStatus(response.getStatus());
			mascomViewAccount.setTerminalUsernameMsisdn(response.getTerminalUserName());
			mascomViewAccount.setDateLogged(new Date());
			transactionDao.update(mascomViewAccount);
		}

	}
}
