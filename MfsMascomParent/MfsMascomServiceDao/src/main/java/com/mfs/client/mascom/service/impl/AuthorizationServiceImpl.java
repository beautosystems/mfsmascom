package com.mfs.client.mascom.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.mascom.dao.SystemConfigDao;
import com.mfs.client.mascom.dao.TransactionDao;
import com.mfs.client.mascom.dto.AuthorizationResponseDto;
import com.mfs.client.mascom.exception.DaoException;
import com.mfs.client.mascom.models.MascomAuthorizationData;
import com.mfs.client.mascom.service.AuthorizationService;
import com.mfs.client.mascom.util.CommonConstant;
import com.mfs.client.mascom.util.HttpsConnectionRequest;
import com.mfs.client.mascom.util.HttpsConnectionResponse;
import com.mfs.client.mascom.util.HttpsConnectorImpl;
import com.mfs.client.mascom.util.JSONToObjectConversion;
import com.mfs.client.mascom.util.MascomCodes;

/**
 * @author - Beauto Systems Pvt. Ltd. Date - 24-March-2020
 * 
 * Purpose : This service is used for Authentication and geneartes the access token.
 * Method : 1.authorizationService
 * 			2.logAuthResponse
 */

@Service("AuthorizationService")
public class AuthorizationServiceImpl implements AuthorizationService {

	private static final Logger LOGGER = Logger.getLogger(AuthorizationServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDao systemConfigDetailsDao;

	// Function to call Auth endpoint and get response
	@SuppressWarnings("unused")
	public AuthorizationResponseDto authorizationService() {

		Map<String, String> configMap = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		AuthorizationResponseDto authorizationResponse = new AuthorizationResponseDto();
		HttpsConnectionResponse httpsConResult = null;

		try {
			// get Config Details from DB
			configMap = systemConfigDetailsDao.getConfigDetailsMap();
			if (configMap == null) {
				throw new Exception("No config Details Found");
			}
			// Append Username and password
			StringBuilder auth = new StringBuilder();
			auth.append(configMap.get(CommonConstant.USERNAME));
			auth.append(":");
			auth.append(configMap.get(CommonConstant.PASSWORD));

			String authenticate = CommonConstant.BEARER
					+ new String(Base64.encodeBase64URLSafeString(auth.toString().getBytes()));
			LOGGER.info("AuthorizationServiceImpl in authorizationService function  authenticate"
					+ authenticate);
			

		    // Set Connection request.
			connectionRequest.setHttpmethodName(CommonConstant.HTTP_METHOD);
			connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)));
			connectionRequest.setServiceUrl(
					configMap.get(CommonConstant.AUTH_URL) + configMap.get(CommonConstant.AUTH_ENDPOINT));

			// set Headers
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);
			headerMap.put(CommonConstant.AUTHORIZATION, authenticate);
			connectionRequest.setHeaders(headerMap);

			// Dummy Response 
			String authResponse ="{\n" + "\"access_token\":\"69c5-abca-416-8a2f-ac5d76a\"\n" + "}";
			
			
			/*HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
			httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(null, connectionRequest);
			String authResponse = httpsConResult.getResponseData();*/
			
			
			// Check Auth response Null or not
			if (null != authResponse) {
				authorizationResponse = (AuthorizationResponseDto) JSONToObjectConversion
						.getObjectFromJson(authResponse, AuthorizationResponseDto.class);
				LOGGER.info("AuthorizationServiceImpl in authorizationService function response "
						+ authorizationResponse);
				// Log Auth Response
				logAuthResponse(authorizationResponse);
			} else {
				LOGGER.error("==>No auth response or auth failed" + authResponse);
				authorizationResponse.setCode(MascomCodes.ER201.getCode());
				authorizationResponse.setMessage(MascomCodes.ER201.getMessage());
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in AuthorizationServiceImpl" + de);
			authorizationResponse.setCode(de.getStatus().getStatusCode());
			authorizationResponse.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in AuthorizationServiceImpl" + e);
			authorizationResponse.setCode(MascomCodes.ER201.getCode());
			authorizationResponse.setMessage(MascomCodes.ER201.getMessage());
		}
		return authorizationResponse;
	}

	// Log Authentication Response in DB
	private void logAuthResponse(AuthorizationResponseDto authorizationResponse) throws DaoException {

		MascomAuthorizationData mascomAuthorizationData = new MascomAuthorizationData();
		mascomAuthorizationData.setAccessToken(authorizationResponse.getAccess_token());
		mascomAuthorizationData.setDateLogged(new Date());
		transactionDao.save(mascomAuthorizationData);
	}
}
