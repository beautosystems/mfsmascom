package com.mfs.client.mascom.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Function")
@XmlType(propOrder = { "name", "msisdn", "remittanceAmount", "brandId", "forexRate", "senderAmountCurrency", "senderAccountNumber", "senderAmount", "mfsTransactionId", "mfsSystemTransactionNumber" })
public class Payment {

	@XmlAttribute(name = "name")
	private String name="PAYMENT";

	private String msisdn;

	private Double remittanceAmount;

	private String brandId;
	
	private String forexRate;

	private String senderAmountCurrency;

	private String senderAccountNumber;

	private String senderAmount;

	private String mfsTransactionId;

	private String mfsSystemTransactionNumber;

	@XmlElement(name = "param1")
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	@XmlElement(name = "param2")
	public Double getRemittanceAmount() {
		return remittanceAmount;
	}

	public void setRemittanceAmount(Double remittanceAmount) {
		this.remittanceAmount = remittanceAmount;
	}
	
	@XmlElement(name = "param5")
	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	
	@XmlElement(name = "param7")
	public String getForexRate() {
		return forexRate;
	}

	public void setForexRate(String forexRate) {
		this.forexRate = forexRate;
	}

	@XmlElement(name = "param8")
	public String getSenderAmountCurrency() {
		return senderAmountCurrency;
	}

	public void setSenderAmountCurrency(String senderAmountCurrency) {
		this.senderAmountCurrency = senderAmountCurrency;
	}

	@XmlElement(name = "param9")
	public String getSenderAccountNumber() {
		return senderAccountNumber;
	}

	public void setSenderAccountNumber(String senderAccountNumber) {
		this.senderAccountNumber = senderAccountNumber;
	}

	@XmlElement(name = "param10")
	public String getSenderAmount() {
		return senderAmount;
	}

	public void setSenderAmount(String senderAmount) {
		this.senderAmount = senderAmount;
	}

	@XmlElement(name = "param11")
	public String getMfsTransactionId() {
		return mfsTransactionId;
	}

	public void setMfsTransactionId(String mfsTransactionId) {
		this.mfsTransactionId = mfsTransactionId;
	}
	
	@XmlElement(name = "param12")
	public String getMfsSystemTransactionNumber() {
		return mfsSystemTransactionNumber;
	}

	public void setMfsSystemTransactionNumber(String mfsSystemTransactionNumber) {
		this.mfsSystemTransactionNumber = mfsSystemTransactionNumber;
	}

	


	

	@Override
	public String toString() {
		return "Payment [name=" + name + ", msisdn=" + msisdn + ", remittanceAmount=" + remittanceAmount + ", brandId="
				+ brandId + ", forexRate=" + forexRate + ", senderAmountCurrency=" + senderAmountCurrency
				+ ", senderAccountNumber=" + senderAccountNumber + ", senderAmount=" + senderAmount
				+ ", mfsAfricaTransactionId=" + mfsTransactionId + ", mfsSystemTransactionNumber="
				+ mfsSystemTransactionNumber + "]";
	}

}
