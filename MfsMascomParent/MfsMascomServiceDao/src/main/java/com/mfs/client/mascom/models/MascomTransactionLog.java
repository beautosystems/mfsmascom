package com.mfs.client.mascom.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mascom_transaction_log")
public class MascomTransactionLog {

	@Id
	@GeneratedValue
	@Column(name = "transaction_id")
	private int transactionId;

	@Column(name = "account_msisdn", length=13)
	private String accountMsisdn;

	@Column(name = "check_only", length=10)
	private String checkOnly;
	
	@Column(name = "remittance_amount", length=15)
	private Double remittanceAmount;

	@Column(name = "brand_id", length=10)
	private String brandId;

	@Column(name = "forex_rate", length=20)
	private String forexRate;

	@Column(name = "sender_amount_currency", length = 10)
	private String senderAmountCurrency;

	@Column(name = "sender_account_number", length = 15)
	private String senderAccountNumber;

	@Column(name = "sender_amount", length = 13)
	private String senderAmount;

	@Column(name = "mfs_transaction_id", length = 50)
	private String mfsTransactionId;

	@Column(name = "mfs_system_transaction_number", length = 30)
	private String mfsSystemTransactionNumber;

	@Column(name = "result", length = 20)
	private String result;

	@Column(name = "message", length = 255)
	private String message;

	@Column(name = "date_logged", length = 20)
	private Date dateLogged;
	
	@Column(name = "tcs_receipt_reference_number", length = 10)
	private String tcsReceiptReferenceNumber;
	
	@Column(name = "sales_order_transaction_number", length = 10)
	private String salesOrderTransactionNumber;

	public int getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}

	public String getAccountMsisdn() {
		return accountMsisdn;
	}

	public void setAccountMsisdn(String accountMsisdn) {
		this.accountMsisdn = accountMsisdn;
	}

	public String getCheckOnly() {
		return checkOnly;
	}

	public void setCheckOnly(String checkOnly) {
		this.checkOnly = checkOnly;
	}

	public Double getRemittanceAmount() {
		return remittanceAmount;
	}

	public void setRemittanceAmount(Double remittanceAmount) {
		this.remittanceAmount = remittanceAmount;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getForexRate() {
		return forexRate;
	}

	public void setForexRate(String forexRate) {
		this.forexRate = forexRate;
	}

	public String getSenderAmountCurrency() {
		return senderAmountCurrency;
	}

	public void setSenderAmountCurrency(String senderAmountCurrency) {
		this.senderAmountCurrency = senderAmountCurrency;
	}

	public String getSenderAccountNumber() {
		return senderAccountNumber;
	}

	public void setSenderAccountNumber(String senderAccountNumber) {
		this.senderAccountNumber = senderAccountNumber;
	}

	public String getSenderAmount() {
		return senderAmount;
	}

	public void setSenderAmount(String senderAmount) {
		this.senderAmount = senderAmount;
	}

	public String getMfsTransactionId() {
		return mfsTransactionId;
	}

	public void setMfsTransactionId(String mfsTransactionId) {
		this.mfsTransactionId = mfsTransactionId;
	}

	public String getMfsSystemTransactionNumber() {
		return mfsSystemTransactionNumber;
	}

	public void setMfsSystemTransactionNumber(String mfsSystemTransactionNumber) {
		this.mfsSystemTransactionNumber = mfsSystemTransactionNumber;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	public String getTcsReceiptReferenceNumber() {
		return tcsReceiptReferenceNumber;
	}

	public void setTcsReceiptReferenceNumber(String tcsReceiptReferenceNumber) {
		this.tcsReceiptReferenceNumber = tcsReceiptReferenceNumber;
	}

	public String getSalesOrderTransactionNumber() {
		return salesOrderTransactionNumber;
	}

	public void setSalesOrderTransactionNumber(String salesOrderTransactionNumber) {
		this.salesOrderTransactionNumber = salesOrderTransactionNumber;
	}

	@Override
	public String toString() {
		return "MascomTransactionLog [transactionId=" + transactionId + ", accountMsisdn=" + accountMsisdn
				+ ", checkOnly=" + checkOnly + ", remittanceAmount=" + remittanceAmount + ", brandId=" + brandId
				+ ", forexRate=" + forexRate + ", senderAmountCurrency=" + senderAmountCurrency
				+ ", senderAccountNumber=" + senderAccountNumber + ", senderAmount=" + senderAmount
				+ ", mfsTransactionId=" + mfsTransactionId + ", mfsSystemTransactionNumber="
				+ mfsSystemTransactionNumber + ", result=" + result + ", message=" + message
				+ ", dateLogged=" + dateLogged + ", tcsReceiptReferenceNumber=" + tcsReceiptReferenceNumber
				+ ", salesOrderTransactionNumber=" + salesOrderTransactionNumber + "]";
	}
	

	
}
