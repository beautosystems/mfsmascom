package com.mfs.client.mascom.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mfs.client.mascom.dao.SystemConfigDao;
import com.mfs.client.mascom.dao.TransactionDao;
import com.mfs.client.mascom.dto.GetSalesOrderRequestDto;
import com.mfs.client.mascom.dto.GetSalesOrderResponseDto;
import com.mfs.client.mascom.exception.DaoException;
import com.mfs.client.mascom.models.MascomAuthorizationData;
import com.mfs.client.mascom.models.MascomSalesOrderTransactionDetails;
import com.mfs.client.mascom.service.GetSalesOrderDetailsService;
import com.mfs.client.mascom.util.CommonConstant;
import com.mfs.client.mascom.util.HttpsConnectionRequest;
import com.mfs.client.mascom.util.HttpsConnectionResponse;
import com.mfs.client.mascom.util.HttpsConnectorImpl;
import com.mfs.client.mascom.util.MascomCodes;
import com.mfs.client.mascom.util.XmlParsingService;

/**
 * @author - Beauto Systems Pvt. Ltd. Date - 27-March-2020
 * 
 * Purpose : This Service is used to get the sales order transaction response details.
 * Methods : 1.SalesOrderDetails
 * 			 2.logSalesOrderResponse
 */

@Service("GetSalesOrderDetailsService")
public class GetSalesOrderDetailsServiceImpl implements GetSalesOrderDetailsService {

	private static final Logger LOGGER = Logger.getLogger(GetSalesOrderDetailsServiceImpl.class);

	@Autowired
	TransactionDao transactionDao;

	@Autowired
	SystemConfigDao systemConfigDetailsDao;

	// Function to call the endpoint url and get the response.
	public GetSalesOrderResponseDto salesOrderDetails(GetSalesOrderRequestDto requestDto) {

		GetSalesOrderResponseDto response = null;
		Map<String, String> configMap;
		XmlParsingService parse = new XmlParsingService();
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;

		try {
			
			// Get Last Record Details of Access Token
			MascomAuthorizationData authorizationData = transactionDao.getLastRecordByAccessToken();
			if(authorizationData==null) {
				response = new GetSalesOrderResponseDto();
				response.setCode(MascomCodes.ER214.getCode());
				response.setMessage(MascomCodes.ER214.getMessage());
			}
			else {
			// get Config Details from DB
			configMap = systemConfigDetailsDao.getConfigDetailsMap();
			if (configMap == null) {
				throw new Exception("No config Details Found");
			}
			requestDto.setUserName(configMap.get(CommonConstant.USERNAME));
			requestDto.setPassword(configMap.get(CommonConstant.PASSWORD));
			requestDto.setTerminalType(configMap.get(CommonConstant.SALES_TERMINAL_TYPE));
			LOGGER.info("GetSalesOrderDetailsServiceImpl in salesOrderDetails function " + requestDto);

			// Convert Request To Xml String
			String xmlSalesRequest = parse.objectToXml(requestDto);
			LOGGER.info("GetSalesOrderDetailsServiceImpl in salesOrderDetails function XML request " + xmlSalesRequest);

			// Set Header Parameters
			Map<String, String> headerMap = new HashMap<String, String>();
			headerMap.put(CommonConstant.AUTHORIZATION,
					CommonConstant.BEARER + authorizationData.getAccessToken());
			headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);


			// Set Connection request.
			/*connectionRequest.setHttpmethodName(CommonConstant.HTTP_METHOD);	
			connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT))); 
			connectionRequest.setServiceUrl(configMap.get(CommonConstant.MASCOM_URL) + 
					  						 (configMap.get(CommonConstant.MASCOM_SALESORDER_ENDPOINT)));
			connectionRequest.setHeaders(headerMap);*/

			// Dummy Response
			String serviceResponse = "<?xml version=\"1.0\"?>\n" 
					+ "<TCSReply>\n"
					+ "<Result>0</Result>\n"
					+ "<Message>Get Sales order details Successfull.Reference 6789</Message>\n"
					+ "<param1>112345</param1>\n"
					+ "<param15>Success</param15>\n" 
					+ "<param16>2020-03-27</param16>\n"
					+ "<param18>123456</param18>\n" 
					+ "<param20>100</param20>\n" 
					+ "<param21>XYZ</param21>\n"
					+ "<param22>111</param22>\n" 
					+ "<param23>ABC</param23>\n" 
					+ "<param24>2020-03-27</param24>\n"
					+ "</TCSReply>";
			LOGGER.info("GetSalesOrderDetailsServiceImpl in salesOrderDetails function XML Response" + serviceResponse);

			// Call Mascom
			/*HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
			httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(xmlSalesRequest, connectionRequest);
			String serviceResponse= httpsConResult.getResponseData();			
			LOGGER.info("GetSalesOrderDetailsServiceImpl in salesOrderDetails function XML Response "+ serviceResponse);*/ 
				
			response = (GetSalesOrderResponseDto) parse.xmlToObjectResponse(new GetSalesOrderResponseDto(),
					serviceResponse);
			LOGGER.info("GetSalesOrderDetailsServiceImpl in salesOrderDetails function " + response);
				
				// Check Sales order response null or not
				if (response != null) {
					logSalesOrderResponse(requestDto, response);
				} else {
					response = new GetSalesOrderResponseDto();
					response.setCode(MascomCodes.ER201.getCode());
					response.setMessage(MascomCodes.ER201.getMessage());
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in GetSalesOrderDetailsServiceImpl" + de);
			response = new GetSalesOrderResponseDto();
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			LOGGER.error("==>Exception in GetSalesOrderDetailsServiceImpl" + e);
			response = new GetSalesOrderResponseDto();
			response.setCode(MascomCodes.ER201.getCode());
			response.setMessage(MascomCodes.ER201.getMessage());
		}
		return response;
	}

	// Log Sales order details
	private void logSalesOrderResponse(GetSalesOrderRequestDto requestDto, GetSalesOrderResponseDto response) throws DaoException {

		// get sales order details by mfstransId
		MascomSalesOrderTransactionDetails mascomSalesOrderTransactionDetails1 = transactionDao.getByMfsIdOrSalesOrderId(requestDto.getFunction().getMfsTransactionId());
		
		// Check Mascom SalesOrderTransactionDetails null or not.
		if(mascomSalesOrderTransactionDetails1==null) {
		MascomSalesOrderTransactionDetails mascomSalesOrderTransactionDetails = new MascomSalesOrderTransactionDetails();
		mascomSalesOrderTransactionDetails.setSalesOrderTransactionId(requestDto.getFunction().getSalesOrderTransactionId());
		mascomSalesOrderTransactionDetails.setMfsTransactionId(requestDto.getFunction().getMfsTransactionId());
		mascomSalesOrderTransactionDetails.setSalesOrderTransactionNumber(response.getSalesOrderTransactionNumber());
		mascomSalesOrderTransactionDetails.setSalesOrderStatus(response.getSalesOrderStatus());
		mascomSalesOrderTransactionDetails.setSalesOrderDate(response.getSalesOrderDate());
		mascomSalesOrderTransactionDetails.setBrandId(response.getBrandId());
		mascomSalesOrderTransactionDetails.setBrandName(response.getBrandName());
		mascomSalesOrderTransactionDetails.setServiceName(response.getServiceName());
		mascomSalesOrderTransactionDetails.setServiceId(response.getServiceId());
		mascomSalesOrderTransactionDetails.setSalesOrderDateExtendedForm(response.getSalesOrderDateExtendedForm());
		mascomSalesOrderTransactionDetails.setDateLogged(new Date());
		transactionDao.save(mascomSalesOrderTransactionDetails);
	}else {
		mascomSalesOrderTransactionDetails1.setSalesOrderTransactionNumber(response.getSalesOrderTransactionNumber());
		mascomSalesOrderTransactionDetails1.setSalesOrderStatus(response.getSalesOrderStatus());
		mascomSalesOrderTransactionDetails1.setSalesOrderDate(response.getSalesOrderDate());
		mascomSalesOrderTransactionDetails1.setBrandId(response.getBrandId());
		mascomSalesOrderTransactionDetails1.setBrandName(response.getBrandName());
		mascomSalesOrderTransactionDetails1.setServiceName(response.getServiceName());
		mascomSalesOrderTransactionDetails1.setServiceId(response.getServiceId());
		mascomSalesOrderTransactionDetails1.setSalesOrderDateExtendedForm(response.getSalesOrderDateExtendedForm());
		mascomSalesOrderTransactionDetails1.setDateLogged(new Date());
		transactionDao.update(mascomSalesOrderTransactionDetails1);
	}
	}
}