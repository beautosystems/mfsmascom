package com.mfs.client.mascom.dao;

import com.mfs.client.mascom.exception.DaoException;
import com.mfs.client.mascom.models.MascomAuthorizationData;
import com.mfs.client.mascom.models.MascomSalesOrderTransactionDetails;
import com.mfs.client.mascom.models.MascomTransactionLog;
import com.mfs.client.mascom.models.MascomViewAccount;

public interface TransactionDao extends BaseDao

{
	public MascomTransactionLog getDetailByMfsTransactionId(String mfsAfricaTransactionId);

	MascomSalesOrderTransactionDetails getByMfsIdOrSalesOrderId(String mfsTransactionId) throws DaoException;
	
	public MascomViewAccount getByMsisdnOrAccountId(String accountMsisdn,String accountId);

	public MascomAuthorizationData getLastRecordByAccessToken() throws DaoException;
	
}
