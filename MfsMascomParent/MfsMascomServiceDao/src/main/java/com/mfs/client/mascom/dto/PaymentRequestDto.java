package com.mfs.client.mascom.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "TCSRequest")
@XmlType(propOrder = { "userName", "password", "terminalType", "function", "checkOnly" })
public class PaymentRequestDto {

	private String userName;

	private String password;

	private String terminalType;

	private Payment function;

	private String checkOnly;

	@XmlElement(name = "UserName")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@XmlElement(name = "Password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@XmlElement(name = "TerminalType")
	public String getTerminalType() {
		return terminalType;
	}

	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}

	@XmlElement(name = "Function")
	public Payment getFunction() {
		return function;
	}

	public void setFunction(Payment function) {
		this.function = function;
	}

	@XmlElement(name = "CheckOnly")
	public String getCheckOnly() {
		return checkOnly;
	}

	public void setCheckOnly(String checkOnly) {
		this.checkOnly = checkOnly;
	}

	@Override
	public String toString() {
		return "XMLRequestWalletTransactionsPaymentDto [userName=" + userName + ", password=" + password
				+ ", terminalType=" + terminalType + ", function=" + function + ", checkOnly=" + checkOnly + "]";
	}

}
