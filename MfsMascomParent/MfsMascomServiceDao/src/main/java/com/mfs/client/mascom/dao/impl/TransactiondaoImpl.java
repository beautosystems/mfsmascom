package com.mfs.client.mascom.dao.impl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import com.mfs.client.mascom.dao.TransactionDao;
import com.mfs.client.mascom.dto.ResponseStatus;
import com.mfs.client.mascom.exception.DaoException;
import com.mfs.client.mascom.models.MascomAuthorizationData;
import com.mfs.client.mascom.models.MascomSalesOrderTransactionDetails;
import com.mfs.client.mascom.models.MascomTransactionLog;
import com.mfs.client.mascom.models.MascomViewAccount;
import com.mfs.client.mascom.util.MascomCodes;

@Repository("TransactionDao")
@EnableTransactionManagement
public class TransactiondaoImpl extends BaseDaoImpl implements TransactionDao {

	@Autowired
	SessionFactory sessionFactory;

	private static final Logger LOGGER = Logger.getLogger(TransactiondaoImpl.class);

	@Transactional
	public MascomTransactionLog getDetailByMfsTransactionId(String mfsTransactionId) {
		MascomTransactionLog mascomTransactionLog = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			mascomTransactionLog = (MascomTransactionLog) session
					.createQuery("From MascomTransactionLog where mfsTransactionId=:mfsTransactionId")
					.setParameter("mfsTransactionId", mfsTransactionId).uniqueResult();

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(MascomCodes.ER212.getMessage());
			status.setStatusCode(MascomCodes.ER212.getCode());

		}
		return mascomTransactionLog;
	}

	
	@Transactional
	public MascomSalesOrderTransactionDetails getByMfsIdOrSalesOrderId(String mfsTransactionId) throws DaoException {
		MascomSalesOrderTransactionDetails mascomSalesOrderTransactionDetails = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			mascomSalesOrderTransactionDetails = 
					(MascomSalesOrderTransactionDetails) session.createQuery
		("From MascomSalesOrderTransactionDetails where  mfsTransactionId=:mfsTransactionId")
					.setParameter("mfsTransactionId", mfsTransactionId).uniqueResult();
		}
		catch(Exception e)
		{
			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(MascomCodes.ER210.getMessage());
			status.setStatusCode(MascomCodes.ER210.getCode());
			throw new DaoException(status);
		}
		
		return mascomSalesOrderTransactionDetails;
}
	
	
	//
	@Transactional
	public MascomViewAccount getByMsisdnOrAccountId(String accountMsisdn,String accountId) {

		MascomViewAccount mascomViewAccount=null;
		
		Session session=sessionFactory.getCurrentSession();		
		try {
			
			mascomViewAccount = (MascomViewAccount) session
					.createQuery("From MascomViewAccount where accountMsisdn=:accountMsisdn or accountId=:accountId ")
					.setParameter("accountMsisdn",accountMsisdn)
					.setParameter("accountId",accountId)
					.uniqueResult();
	
		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(MascomCodes.ER211.getMessage());
			status.setStatusCode(MascomCodes.ER211.getCode());
		}
		return mascomViewAccount;
		
		
	}

	@Transactional
	public MascomAuthorizationData getLastRecordByAccessToken() throws DaoException {
		MascomAuthorizationData mascomAuthorizationData = null;
		Session session = sessionFactory.getCurrentSession();
		try {
			mascomAuthorizationData = (MascomAuthorizationData) session
					.createQuery("From MascomAuthorizationData order by auth_id  DESC").list().get(0);

		} catch (Exception e) {

			ResponseStatus status = new ResponseStatus();
			status.setStatusMessage(MascomCodes.ER215.getMessage());
			status.setStatusCode(MascomCodes.ER215.getCode());
			throw new DaoException(status);

		}
		return mascomAuthorizationData;
	}


	


	
}
