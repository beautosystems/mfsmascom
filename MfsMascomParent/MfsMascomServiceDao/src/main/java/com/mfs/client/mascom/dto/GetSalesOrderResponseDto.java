
package com.mfs.client.mascom.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlRootElement(name = "TCSReply")
@XmlType(propOrder = { "code","message","salesOrderTransactionNumber", "salesOrderStatus", "salesOrderDate", "mfsTransactionId",
		"brandId", "brandName", "serviceId", "serviceName", "salesOrderDateExtendedForm" })
public class GetSalesOrderResponseDto {

	private String code;
	private String message;
	private String salesOrderTransactionNumber;
	private String salesOrderStatus;
	private String salesOrderDate;
	private String mfsTransactionId;
	private String brandId;
	private String brandName;
	private String serviceId;
	private String serviceName;
	private String salesOrderDateExtendedForm;

	@XmlElement(name = "param1")
	public String getSalesOrderTransactionNumber() {
		return salesOrderTransactionNumber;
	}

	public void setSalesOrderTransactionNumber(String salesOrderTransactionNumber) {
		this.salesOrderTransactionNumber = salesOrderTransactionNumber;
	}

	@XmlElement(name = "param15")
	public String getSalesOrderStatus() {
		return salesOrderStatus;
	}

	public void setSalesOrderStatus(String salesOrderStatus) {
		this.salesOrderStatus = salesOrderStatus;
	}

	@XmlElement(name = "param16")
	public String getSalesOrderDate() {
		return salesOrderDate;
	}

	public void setSalesOrderDate(String salesOrderDate) {
		this.salesOrderDate = salesOrderDate;
	}

	@XmlElement(name = "param18")
	public String getMfsTransactionId() {
		return mfsTransactionId;
	}

	public void setMfsTransactionId(String mfsTransactionId) {
		this.mfsTransactionId = mfsTransactionId;
	}

	@XmlElement(name = "param20")
	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	@XmlElement(name = "param21")
	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	@XmlElement(name = "param22")
	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	@XmlElement(name = "param23")
	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@XmlElement(name = "param24")
	public String getSalesOrderDateExtendedForm() {
		return salesOrderDateExtendedForm;
	}

	public void setSalesOrderDateExtendedForm(String salesOrderDateExtendedForm) {
		this.salesOrderDateExtendedForm = salesOrderDateExtendedForm;
	}

	@XmlElement(name = "Result")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@XmlElement(name = "Message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "GetSalesOrderResponseDto [code=" + code + ", message=" + message + ", salesOrderTransactionNumber="
				+ salesOrderTransactionNumber + ", salesOrderStatus=" + salesOrderStatus + ", salesOrderDate="
				+ salesOrderDate + ", mfsTransactionId=" + mfsTransactionId + ", brandId=" + brandId + ", brandName="
				+ brandName + ", serviceId=" + serviceId + ", serviceName=" + serviceName
				+ ", salesOrderDateExtendedForm=" + salesOrderDateExtendedForm + "]";
	}

	
}
