/**
 * 
 */
package com.mfs.client.mascom.util;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.log4j.Logger;

public class HttpsConnectorImpl {

	private static final Logger LOGGER = Logger.getLogger(HttpsConnectorImpl.class);

	private static final int CHECK_RESPONSE_CODE201 = 201;
	private static final int CHECK_RESPONSE_CODE200 = 200;
	private static final int CHECK_RESPONSE_CODE202 = 202;

	/**
	 * @param connectURL
	 * @param data
	 * @return
	 * @throws UnknownHostException
	 * @throws IOException
	 * @throws ProtocolException
	 * @throws MTAdapterConEx
	 */

	public HttpsConnectionResponse connectionUsingHTTPS(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException {
		LOGGER.info("Start of connectionUsingHTTPS() in HttpsConnectorImpl");
		SSLContext sslContext = null;

		HttpsConnectionResponse connectionResponse = new HttpsConnectionResponse();
		String responseData = null;
		try {
			sslContext = SSLContext.getInstance("TLSv1.2");
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (Exception e) {
			LOGGER.error("Exception occured while setting SSL factory", e);
		}

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();
		LOGGER.info("Setting SSL Socket Factory...");

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);
		httpsConnection.setReadTimeout(60000);

		if (connectionData != null) {
			LOGGER.info("connectionData length :: " + connectionData.length());
			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}
		int responseCode = httpsConnection.getResponseCode();

		LOGGER.info("Response Status from connecting partner -->" + responseCode);

		BufferedReader httpsResponse = null;

		if (responseCode == MascomResponseCodes.S201.getCode() || responseCode == MascomResponseCodes.S200.getCode()
				|| responseCode == MascomResponseCodes.S202.getCode()) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else if (responseCode == MascomResponseCodes.ER400.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			LOGGER.info("ErrorMessage  " + errMessage);

			connectionResponse.setCode(ResponseCodes.INVALID_REQUEST.getCode());

			connectionResponse.setTxMessage(ResponseCodes.INVALID_REQUEST.getMessage() + errMessage);

			System.out.println("Error Message for SendToup:" + getInputAsString(httpsConnection.getErrorStream()));

			LOGGER.info("Error Message for SendToup:" + getInputAsString(httpsConnection.getErrorStream()));

		} else if (responseCode == MascomResponseCodes.ER401.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			LOGGER.info("ErrorMessage  " + errMessage);

			connectionResponse.setCode(ResponseCodes.AUTHENTICATION_FAILURE.getCode());

			connectionResponse.setTxMessage(ResponseCodes.AUTHENTICATION_FAILURE.getMessage() + errMessage);

			System.out.println("Error Message for SendToup:" + getInputAsString(httpsConnection.getErrorStream()));

			LOGGER.info("Error Message for SendToup:" + getInputAsString(httpsConnection.getErrorStream()));

		} else if (responseCode == MascomResponseCodes.ER403.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			LOGGER.info("ErrorMessage  " + errMessage);

			connectionResponse.setCode(ResponseCodes.NOT_AUTHORIZED.getCode());

			connectionResponse.setTxMessage(ResponseCodes.NOT_AUTHORIZED.getMessage() + errMessage);

		} else if (responseCode == MascomResponseCodes.ER404.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			LOGGER.info("ErrorMessage  " + errMessage);

			connectionResponse.setCode(ResponseCodes.RESOURCE_NOT_FOUND.getCode());

			connectionResponse.setTxMessage(ResponseCodes.RESOURCE_NOT_FOUND.getMessage() + errMessage);

		} else if (responseCode == MascomResponseCodes.ER405.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			LOGGER.info("ErrorMessage  " + errMessage);

			connectionResponse.setCode(ResponseCodes.METHOD_NOT_SUPPORTED.getCode());

			connectionResponse.setTxMessage(ResponseCodes.METHOD_NOT_SUPPORTED.getMessage() + errMessage);

		} else if (responseCode == MascomResponseCodes.ER406.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			LOGGER.info("ErrorMessage  " + errMessage);

			connectionResponse.setCode(ResponseCodes.MEDIA_TYPE_NOT_ACCEPTABLE.getCode());

			connectionResponse.setTxMessage(ResponseCodes.MEDIA_TYPE_NOT_ACCEPTABLE.getMessage() + errMessage);

		} else if (responseCode == MascomResponseCodes.ER415.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			LOGGER.info("ErrorMessage  " + errMessage);

			connectionResponse.setCode(ResponseCodes.UNSUPPORTED_MEDIA_TYPE.getCode());

			connectionResponse.setTxMessage(ResponseCodes.UNSUPPORTED_MEDIA_TYPE.getMessage() + errMessage);

		} else if (responseCode == MascomResponseCodes.ER422.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			LOGGER.info("ErrorMessage  " + errMessage);

			connectionResponse.setCode(ResponseCodes.UNPROCCESSABLE_ENTITY.getCode());

			connectionResponse.setTxMessage(ResponseCodes.UNPROCCESSABLE_ENTITY.getMessage() + errMessage);

		} else if (responseCode == MascomResponseCodes.ER500.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			LOGGER.info("ErrorMessage  " + errMessage);

			connectionResponse.setCode(ResponseCodes.INTERNAL_SERVER_ERROR.getCode());

			connectionResponse.setTxMessage(ResponseCodes.INTERNAL_SERVER_ERROR.getMessage() + errMessage);

			System.out.println("err" + getInputAsString(httpsConnection.getErrorStream()));

		} else if (responseCode == MascomResponseCodes.ER503.getCode()) {

			String errMessage = getInputAsString(httpsConnection.getErrorStream());

			LOGGER.debug("ConnectionData " + connectionData + " ErrorMessage  " + errMessage);

			LOGGER.info("ErrorMessage  " + errMessage);

			connectionResponse.setCode(ResponseCodes.SERVICE_UNAVAILABLE.getCode());

			connectionResponse.setTxMessage(ResponseCodes.SERVICE_UNAVAILABLE.getMessage() + errMessage);
		} else {

			if (httpsConnection != null) {
				httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getErrorStream()));
			}

		}

		String output = null;
		if (httpsResponse != null) {
			output = readConnectionDataWithBuffer(httpsResponse);
		}

		if (httpsResponse != null) {
			httpsResponse.close();
		}

		LOGGER.debug("Response -->" + output);
		connectionResponse.setResponseData(output);

		LOGGER.info("End of connectionUsingHTTPS() in HttpsConnectorImpl");
		return connectionResponse;
	}

	private BufferedReader connectionUsingHTTPSRetry(String connectionData,
			final HttpsConnectionRequest httpsConnectionRequest) throws UnknownHostException, IOException {
		LOGGER.info("Start of connectionUsingHTTPSRetry() in HttpsConnectorImpl");
		SSLContext sslContext = null;

		try {
			sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, new javax.net.ssl.TrustManager[] { new TrustAllTrustManager() },
					new java.security.SecureRandom());
		} catch (Exception e) {
			LOGGER.error("Exception occured while setting SSL factory", e);
		}

		URL httpsUrl = new URL(httpsConnectionRequest.getServiceUrl());

		sslContext.getSocketFactory().createSocket(httpsUrl.getHost(), httpsConnectionRequest.getPort());

		HttpsURLConnection httpsConnection = (HttpsURLConnection) httpsUrl.openConnection();

		LOGGER.info("Setting SSL Socket Factory...");

		httpsConnection.setSSLSocketFactory(sslContext.getSocketFactory());

		httpsConnection.setRequestMethod(httpsConnectionRequest.getHttpmethodName());

		httpsConnection.setReadTimeout(60000);

		httpsConnection.setDoOutput(true);
		httpsConnection.setDoInput(true);

		setHeaderPropertiesToHTTPSConnection(httpsConnectionRequest, connectionData, httpsConnection);

		if (connectionData != null) {

			DataOutputStream wr = new DataOutputStream(httpsConnection.getOutputStream());

			wr.writeBytes(connectionData);
			wr.flush();
			wr.close();
		}
		int responseCode = httpsConnection.getResponseCode();

		LOGGER.info("Response Status from connecting partner -->" + responseCode);

		BufferedReader httpsResponse = null;

		if (responseCode == CHECK_RESPONSE_CODE200) {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getInputStream()));

		} else {

			httpsResponse = new BufferedReader(new InputStreamReader(httpsConnection.getErrorStream()));

		}

		String output = readConnectionDataWithBuffer(httpsResponse);

		if (output == null || output.isEmpty()) {
			if (responseCode == CHECK_RESPONSE_CODE200) {
				output = "OK";
			}
		}

		LOGGER.info("End of connectionUsingHTTPSRetry() in HttpsConnectorImpl");
		return httpsResponse;
	}

	public static void wait(int seconds) {
		try {
			LOGGER.info("Waiting " + seconds + " seconds ..");
			Thread.sleep(seconds * 1000);
		} catch (InterruptedException e) {
			LOGGER.error("Exception Occured in HttpsConnectorImpl :: " + e);
		}
	}

	private String readConnectionDataWithBuffer(BufferedReader httpsResponse) throws IOException {
		String output;
		String responseLine;
		StringBuffer response = new StringBuffer();

		while ((responseLine = httpsResponse.readLine()) != null) {
			response.append(responseLine);
		}

		output = response.toString();
		return output;
	}

	private void setHeaderPropertiesToHTTPSConnection(final HttpsConnectionRequest httpsConnectionRequest,
			String connectionData, HttpsURLConnection httpsConnection) {

		setHeadersToHTTPSConnection(httpsConnectionRequest.getHeaders(), httpsConnection);

		if (connectionData != null && connectionData.length() > 0) {

			httpsConnection.setRequestProperty("Content-Length", String.valueOf(connectionData.length()));
		} else {
			httpsConnection.setRequestProperty("Content-Length", "0");
		}

	}

	private void setHeadersToHTTPSConnection(Map<String, String> headers, HttpsURLConnection httpsConnection) {

		if (headers != null && !headers.isEmpty() && httpsConnection != null) {

			for (Entry<String, String> entry : headers.entrySet()) {

				httpsConnection.setRequestProperty(entry.getKey(), entry.getValue());
			}
		}
	}

	private static String getInputAsString(InputStream paramInputStream) {
		Scanner localScanner = new Scanner(paramInputStream);
		return localScanner.useDelimiter("\\A").hasNext() ? localScanner.next() : "";
	}

}