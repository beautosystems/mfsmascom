package com.mfs.client.mascom.dao;

import java.util.Map;

import com.mfs.client.mascom.exception.DaoException;

public interface SystemConfigDao {

	public Map<String, String> getConfigDetailsMap() throws DaoException;
	
}
