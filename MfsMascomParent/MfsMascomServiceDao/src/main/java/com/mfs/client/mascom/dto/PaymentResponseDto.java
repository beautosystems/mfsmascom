package com.mfs.client.mascom.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@XmlRootElement(name = "TCSReply")
public class PaymentResponseDto {

	private String code;
	private String message;
	private String tcsReceiptReferenceNumber;
	private Double remittanceAmount;
	private String mfsTransactionId;
	private String salesOrderTransactionNumber;

	@XmlElement(name = "Result")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@XmlElement(name = "Message")
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@XmlElement(name = "param1")
	public String getTcsReceiptReferenceNumber() {
		return tcsReceiptReferenceNumber;
	}

	public void setTcsReceiptReferenceNumber(String tcsReceiptReferenceNumber) {
		this.tcsReceiptReferenceNumber = tcsReceiptReferenceNumber;
	}

	@XmlElement(name = "param2")
	public Double getRemittanceAmount() {
		return remittanceAmount;
	}

	public void setRemittanceAmount(Double remittanceAmount) {
		this.remittanceAmount = remittanceAmount;
	}

	@XmlElement(name = "param10")
	public String getMfsTransactionId() {
		return mfsTransactionId;
	}

	public void setMfsTransactionId(String mfsTransactionId) {
		this.mfsTransactionId = mfsTransactionId;
	}

	@XmlElement(name = "param11")
	public String getSalesOrderTransactionNumber() {
		return salesOrderTransactionNumber;
	}

	public void setSalesOrderTransactionNumber(String salesOrderTransactionNumber) {
		this.salesOrderTransactionNumber = salesOrderTransactionNumber;
	}

	@Override
	public String toString() {
		return "PaymentResponseDto [message=" + message + ", tcsReceiptReferenceNumber="
				+ tcsReceiptReferenceNumber + ", remittanceAmount=" + remittanceAmount + ", mfsTransactionId="
				+ mfsTransactionId + ", salesOrderTransactionNumber=" + salesOrderTransactionNumber + ", code=" + code
				+ "]";
	}

}
