package com.mfs.client.mascom.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name="Function")
@XmlType(propOrder = { "name","accountMsisdn","accountId"})
public class ViewAccountType {
	
	@XmlAttribute(name = "name")
	private String name="VIEWACCOUNTTYPE";
	
	private String accountMsisdn;
	private String accountId;
	
	
	@XmlElement(name = "param1")
	public String getAccountMsisdn() {
		return accountMsisdn;
	}
	public void setAccountMsisdn(String accountMsisdn) {
		this.accountMsisdn = accountMsisdn;
	}
	
	@XmlElement(name = "param1")
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	
	@Override
	public String toString() {
		return "ViewAccountType [name=" + name + ", accountMsisdn=" + accountMsisdn + ", accountId=" + accountId + "]";
	}
	
	
	

	
	
}
