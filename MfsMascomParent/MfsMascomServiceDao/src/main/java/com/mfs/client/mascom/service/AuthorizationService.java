package com.mfs.client.mascom.service;

import com.mfs.client.mascom.dto.AuthorizationResponseDto;

public interface AuthorizationService {
	
	public AuthorizationResponseDto authorizationService();

}
