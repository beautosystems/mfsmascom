package com.mfs.client.mascom.service.impl;

import java.util.Date;
import java.util.Map;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.mfs.client.mascom.dao.SystemConfigDao;
import com.mfs.client.mascom.dao.TransactionDao;
import com.mfs.client.mascom.dto.PaymentRequestDto;
import com.mfs.client.mascom.dto.PaymentResponseDto;
import com.mfs.client.mascom.exception.DaoException;
import com.mfs.client.mascom.models.MascomAuthorizationData;
import com.mfs.client.mascom.models.MascomTransactionError;
import com.mfs.client.mascom.models.MascomTransactionLog;
import com.mfs.client.mascom.service.PaymentService;
import com.mfs.client.mascom.util.CommonConstant;
import com.mfs.client.mascom.util.HttpsConnectionRequest;
import com.mfs.client.mascom.util.HttpsConnectionResponse;
import com.mfs.client.mascom.util.MascomCodes;
import com.mfs.client.mascom.util.XmlParsingService;

/*
* @author Pallavi Gohite
* PaymentService.java
* 27-03-2020
*
* purpose: this class is used for save and update payment transaction
*
* Methods
* 1. paymentDetail
* 2. logResponse
*
*/

@Service("PaymentService")
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	private TransactionDao transactionDao;

	@Autowired
	private SystemConfigDao systemConfigDao;

	private static final Logger LOGGER = Logger.getLogger(PaymentServiceImpl.class);

	public PaymentResponseDto paymentDetail(PaymentRequestDto request) {
		PaymentResponseDto response = null;
		String serviceResponse = null;
		XmlParsingService xmlParsingServiceImpl = new XmlParsingService();
		Map<String, String> systemconfig = null;
		HttpsConnectionRequest connectionRequest = new HttpsConnectionRequest();
		HttpsConnectionResponse httpsConResult = null;

		try {
			// Get Last Record Details of Access Token
			MascomAuthorizationData authorizationData = transactionDao.getLastRecordByAccessToken();
			if (authorizationData == null) {
				response = new PaymentResponseDto();
				response.setCode(MascomCodes.ER214.getCode());
				response.setMessage(MascomCodes.ER214.getMessage());
			} else {
				MascomTransactionLog transactionLog = transactionDao
						.getDetailByMfsTransactionId(request.getFunction().getMfsTransactionId());
				if (transactionLog != null) {
					response = new PaymentResponseDto();
					response.setCode(MascomCodes.ER216.getCode());
					response.setMessage(MascomCodes.ER216.getMessage());
				} else {
					logRequest(request);
					// Get Data from system config
					systemconfig = systemConfigDao.getConfigDetailsMap();
					if (systemconfig == null) {
						throw new Exception("No Config Details in DB");
					}

					request.setUserName(systemconfig.get(CommonConstant.USERNAME));
					request.setPassword(systemconfig.get(CommonConstant.PASSWORD));
					request.setTerminalType(systemconfig.get(CommonConstant.TERMINAL_TYPE));
					LOGGER.info("PaymentServiceImpl in paymentDetail function request" + request);
					String xml = xmlParsingServiceImpl.objectToXml(request);
					LOGGER.info("PaymentServiceImpl in paymentDetail function xml request" + xml);
					// Set Header Parameters

					/*
					 * Map<String, String> headerMap = new HashMap<String, String>();
					 * headerMap.put(CommonConstant.AUTHORIZATION, CommonConstant.BEARER +
					 * authorizationData.getAccessToken());
					 * headerMap.put(CommonConstant.CONTENT_TYPE, CommonConstant.TEXT_XML);
					 */

					// Set Connection request.
					/*
					 * connectionRequest.setHttpmethodName(CommonConstant.HTTP_METHOD);
					 * connectionRequest.setPort(Integer.parseInt(configMap.get(CommonConstant.PORT)
					 * )); connectionRequest.setServiceUrl(configMap.get(CommonConstant.MASCOM_URL)
					 * + (configMap.get(CommonConstant.MASCOM_PAYMENT_URL)));
					 */
					// Dummy response
					serviceResponse = "<?xml version=\"1.0\"?>\n" 
					+ "<TCSReply>\n" 
							+ "<Result>0</Result>\n"
							+ "<Message>Money transfer successful. Reference: 6789</Message>\n"
							+ "<param1>234</param1>\n" 
							+ "<param2>456</param2>\n" 
							+ "<param10>"+request.getFunction().getMfsTransactionId()+"</param10>\n"
							+ "<param11>1236767</param11>\n" 
					+ "</TCSReply>";

					/*
					 * HttpsConnectorImpl httpsConnectorImpl = new HttpsConnectorImpl();
					 * httpsConResult = httpsConnectorImpl.connectionUsingHTTPS(xml,
					 * connectionRequest); String serviceResponse= httpsConResult.getResponseData();
					 */
					LOGGER.info("PaymentServiceImpl in paymentDetail function  " + serviceResponse);
					response = (PaymentResponseDto) xmlParsingServiceImpl.xmlToObjectResponse(new PaymentResponseDto(),
							serviceResponse);
					LOGGER.info("PaymentServiceImpl in paymentDetail function  " + response);

					// Check for response success or fail transaction
					if (response == null) {
						response = new PaymentResponseDto();
						response.setCode(MascomCodes.ER202.getCode());
						response.setMessage(MascomCodes.ER202.getMessage());
					} else if (response.getCode().equals("0")) {
						logResponse(request, response);
					} else {
						response.getCode();
						response.getMessage();
						errorLogResponse(request, response);
					}
				}
			}
		} catch (DaoException de) {
			LOGGER.error("==>DaoException in PaymentServiceImpl" + de);
			response = new PaymentResponseDto();
			response.setCode(de.getStatus().getStatusCode());
			response.setMessage(de.getStatus().getStatusMessage());
		} catch (Exception e) {
			response = new PaymentResponseDto();
			response.setCode(MascomCodes.ER201.getCode());
			response.setMessage(MascomCodes.ER201.getMessage());
		}

		return response;
	}

	// method for success log request 
	private void logRequest(PaymentRequestDto request) throws DaoException {
		MascomTransactionLog transactionLog = new MascomTransactionLog();
		transactionLog.setCheckOnly(request.getCheckOnly());
		transactionLog.setAccountMsisdn(request.getFunction().getMsisdn());
		transactionLog.setRemittanceAmount(request.getFunction().getRemittanceAmount());
		transactionLog.setBrandId(request.getFunction().getBrandId());
		transactionLog.setForexRate(request.getFunction().getForexRate());
		transactionLog.setSenderAmountCurrency(request.getFunction().getSenderAmountCurrency());
		transactionLog.setSenderAccountNumber(request.getFunction().getSenderAccountNumber());
		transactionLog.setSenderAmount(request.getFunction().getSenderAmount());
		transactionLog.setMfsTransactionId(request.getFunction().getMfsTransactionId());
		transactionLog.setMfsSystemTransactionNumber(request.getFunction().getMfsSystemTransactionNumber());
		transactionLog.setDateLogged(new Date());
		transactionDao.save(transactionLog);
		
	}

	// method for fail log response and request
	private void errorLogResponse(PaymentRequestDto request, PaymentResponseDto response) throws DaoException {
		MascomTransactionError transactionLogError = new MascomTransactionError();
		transactionLogError.setCheckOnly(request.getCheckOnly());
		transactionLogError.setAccountMsisdn(request.getFunction().getMsisdn());
		transactionLogError.setRemittanceAmount(request.getFunction().getRemittanceAmount());
		transactionLogError.setBrandId(request.getFunction().getBrandId());
		transactionLogError.setForexRate(request.getFunction().getForexRate());
		transactionLogError.setSenderAmountCurrency(request.getFunction().getSenderAmountCurrency());
		transactionLogError.setSenderAccountNumber(request.getFunction().getSenderAccountNumber());
		transactionLogError.setSenderAmount(request.getFunction().getSenderAmount());
		transactionLogError.setMfsTransactionId(request.getFunction().getMfsTransactionId());
		transactionLogError.setMfsSystemTransactionNumber(request.getFunction().getMfsSystemTransactionNumber());
		transactionLogError.setResult(response.getCode());
		transactionLogError.setMessage(response.getMessage());
		transactionLogError.setDateLogged(new Date());
		transactionDao.save(transactionLogError);
	}

	// method for success log response 
	private void logResponse(PaymentRequestDto request, PaymentResponseDto response) throws DaoException {

		MascomTransactionLog transactionLog = transactionDao.getDetailByMfsTransactionId(request.getFunction().getMfsTransactionId());
		transactionLog.setResult(response.getCode());
		transactionLog.setMessage(response.getMessage());
		transactionLog.setTcsReceiptReferenceNumber(response.getTcsReceiptReferenceNumber());
		transactionLog.setRemittanceAmount(response.getRemittanceAmount());
		transactionLog.setSalesOrderTransactionNumber(response.getSalesOrderTransactionNumber());
		transactionLog.setDateLogged(new Date());
		transactionDao.update(transactionLog);

	}

}
