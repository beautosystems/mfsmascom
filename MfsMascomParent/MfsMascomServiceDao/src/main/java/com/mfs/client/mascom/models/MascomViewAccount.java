package com.mfs.client.mascom.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "macom_view_account")
public class MascomViewAccount {

	@Id
	@GeneratedValue
	@Column(name = "view_account_id")
	private int viewAccountId;

	@Column(name = "account_msisdn ")
	private String accountMsisdn;

	@Column(name = "account_id ")
	private String accountId;

	@Column(name = "account_name ")
	private String accountName;

	@Column(name = "account_customer_type ")
	private String accountCustomerType;

	@Column(name = "account_layer ")
	private String accountLayer;

	@Column(name = "account_group ")
	private String accountGroup;

	@Column(name = "account_status ")
	private String accountStatus;

	@Column(name = "terminal_username_msisdn ")
	private String terminalUsernameMsisdn;

	@Column(name = "date_logged ")
	private Date dateLogged;

	public int getViewAccountId() {
		return viewAccountId;
	}

	public void setViewAccountId(int viewAccountId) {
		this.viewAccountId = viewAccountId;
	}

	public String getAccountMsisdn() {
		return accountMsisdn;
	}

	public void setAccountMsisdn(String accountMsisdn) {
		this.accountMsisdn = accountMsisdn;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getAccountCustomerType() {
		return accountCustomerType;
	}

	public void setAccountCustomerType(String accountCustomerType) {
		this.accountCustomerType = accountCustomerType;
	}

	public String getAccountLayer() {
		return accountLayer;
	}

	public void setAccountLayer(String accountLayer) {
		this.accountLayer = accountLayer;
	}

	public String getAccountGroup() {
		return accountGroup;
	}

	public void setAccountGroup(String accountGroup) {
		this.accountGroup = accountGroup;
	}

	public String getAccountStatus() {
		return accountStatus;
	}

	public void setAccountStatus(String accountStatus) {
		this.accountStatus = accountStatus;
	}

	public String getTerminalUsernameMsisdn() {
		return terminalUsernameMsisdn;
	}

	public void setTerminalUsernameMsisdn(String terminalUsernameMsisdn) {
		this.terminalUsernameMsisdn = terminalUsernameMsisdn;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "MascomViewAccount [viewAccountId=" + viewAccountId + ", accountMsisdn=" + accountMsisdn + ", accountId="
				+ accountId + ", accountName=" + accountName + ", accountCustomerType=" + accountCustomerType
				+ ", accountLayer=" + accountLayer + ", accountGroup=" + accountGroup + ", accountStatus="
				+ accountStatus + ", terminalUsernameMsisdn=" + terminalUsernameMsisdn + ", dateLogged=" + dateLogged
				+ "]";
	}

}