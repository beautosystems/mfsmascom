package com.mfs.client.mascom.util;

public class CommonConstant {

	public static String base_url = "base_url";
	public static String SEND_OPERATION = "send_operation";
	public static final String GET_TRANS_URL = "transaction_details";

	public static final String CONTENT_TYPE = "Content-type";
	public static final String APPLICATION_JSON = "application/json";
	public static final String TEXT_XML = "text/xml";

	public static final String USERNAME = "username";
	public static final String PASSWORD = "password";
	public static final String PORT = "port";
	public static final String HTTP_METHOD = "POST";

	public static final String AUTH_URL = "auth_url";
	public static final String AUTH_ENDPOINT = "auth_endpoint";
	public static final String AUTHORIZATION = "Authorization";

	// for payment service
	public static final String MASCOM_PAYMENT_URL=null;

	// Sales order
	public static final String MASCOM_URL = "mascom_url";
	public static final String MASCOM_SALESORDER_ENDPOINT = "masom_sales_order_endpoint";
	public static final String SALES_TERMINAL_TYPE = "sales_terminal_type";

	//for view account type service
	public static final String TERMINAL_TYPE = "terminal_type";
	public static final String VIEW_ACCOUNT_TYPE_URL = null;
	public static final String NAME = "VIEWACCOUNTTYPE";
	public static final String SALES_NAME = "GetSalesOrderDetails";
	public static final String PAYMENT_NAME = "PAYMENT";
	public static final String BEARER = "Bearer ";
	public static final String INVALID_MFSTRANS_ID = "Mfs Transaction Id Field is required";

}
