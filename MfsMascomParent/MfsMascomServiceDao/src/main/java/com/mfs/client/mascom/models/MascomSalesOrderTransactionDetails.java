package com.mfs.client.mascom.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mascom_sales_order_transaction_details")
public class MascomSalesOrderTransactionDetails {

	@Id
	@GeneratedValue
	@Column(name = "sales_order_id")
	private int salesOrderId;

	@Column(name = "sales_order_transaction_id", length = 20)
	private String salesOrderTransactionId;

	@Column(name = "mfs_transaction_id", length = 20)
	private String mfsTransactionId;

	@Column(name = "sales_order_transaction_number", length = 20)
	private String salesOrderTransactionNumber;

	@Column(name = "sales_order_status", length = 20)
	private String salesOrderStatus;

	@Column(name = "sales_order_date", length = 30)
	private String salesOrderDate;

	@Column(name = "brand_id", length = 20)
	private String brandId;

	@Column(name = "brand_name", length = 50)
	private String brandName;

	@Column(name = "service_id", length = 20)
	private String serviceId;

	@Column(name = "service_name", length = 50)
	private String serviceName;

	@Column(name = "sales_order_date_extended", length = 30)
	private String salesOrderDateExtendedForm;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getSalesOrderId() {
		return salesOrderId;
	}

	public void setSalesOrderId(int salesOrderId) {
		this.salesOrderId = salesOrderId;
	}

	public String getSalesOrderTransactionId() {
		return salesOrderTransactionId;
	}

	public void setSalesOrderTransactionId(String salesOrderTransactionId) {
		this.salesOrderTransactionId = salesOrderTransactionId;
	}

	public String getMfsTransactionId() {
		return mfsTransactionId;
	}

	public void setMfsTransactionId(String mfsTransactionId) {
		this.mfsTransactionId = mfsTransactionId;
	}

	public String getSalesOrderTransactionNumber() {
		return salesOrderTransactionNumber;
	}

	public void setSalesOrderTransactionNumber(String salesOrderTransactionNumber) {
		this.salesOrderTransactionNumber = salesOrderTransactionNumber;
	}

	public String getSalesOrderStatus() {
		return salesOrderStatus;
	}

	public void setSalesOrderStatus(String salesOrderStatus) {
		this.salesOrderStatus = salesOrderStatus;
	}

	public String getSalesOrderDate() {
		return salesOrderDate;
	}

	public void setSalesOrderDate(String salesOrderDate) {
		this.salesOrderDate = salesOrderDate;
	}

	public String getBrandId() {
		return brandId;
	}

	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getSalesOrderDateExtendedForm() {
		return salesOrderDateExtendedForm;
	}

	public void setSalesOrderDateExtendedForm(String salesOrderDateExtendedForm) {
		this.salesOrderDateExtendedForm = salesOrderDateExtendedForm;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "MascomSalesOrderTransactionDetails [salesOrderId=" + salesOrderId + ", salesOrderTransactionId="
				+ salesOrderTransactionId + ", mfsTransactionId=" + mfsTransactionId + ", salesOrderTransactionNumber="
				+ salesOrderTransactionNumber + ", salesOrderStatus=" + salesOrderStatus + ", salesOrderDate="
				+ salesOrderDate + ", brandId=" + brandId + ", brandName=" + brandName + ", serviceId=" + serviceId
				+ ", serviceName=" + serviceName + ", salesOrderDateExtendedForm=" + salesOrderDateExtendedForm
				+ ", dateLogged=" + dateLogged + "]";
	}

}
