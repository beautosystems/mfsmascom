package com.mfs.client.mascom.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@XmlRootElement(name="TCSReply")
@XmlType(propOrder = {"code","message","accountMsisdn","accountId","accountName","customerType","accountLayer","group","status","terminalUserName"})
public class ViewAccountTypeResponseDto {
	
	private String code;
	private String message;
	private String accountMsisdn;
	private String accountId;
	private String accountName;
	private String customerType;
	private String accountLayer;
	private String group;
	private String status;
	private String terminalUserName;
	
	@XmlElement(name = "Result")
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	@XmlElement(name = "Message")
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	@XmlElement(name = "param1")
	public String getAccountMsisdn() {
		return accountMsisdn;
	}
	public void setAccountMsisdn(String accountMsisdn) {
		this.accountMsisdn = accountMsisdn;
	}
	@XmlElement(name = "param2")
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	@XmlElement(name = "param3")
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	@XmlElement(name = "param4")
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	
	@XmlElement(name = "param5")
	public String getAccountLayer() {
		return accountLayer;
	}
	public void setAccountLayer(String accountLayer) {
		this.accountLayer = accountLayer;
	}
	
	@XmlElement(name = "param6")
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	
	@XmlElement(name = "param7")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@XmlElement(name = "param8")
	public String getTerminalUserName() {
		return terminalUserName;
	}
	public void setTerminalUserName(String terminalUserName) {
		this.terminalUserName = terminalUserName;
	}
	@Override
	public String toString() {
		return "ViewAccountTypeResponseDto [code=" + code + ", message=" + message + ", accountMsisdn=" + accountMsisdn
				+ ", accountId=" + accountId + ", accountName=" + accountName + ", customerType=" + customerType
				+ ", accountLayer=" + accountLayer + ", group=" + group + ", status=" + status + ", terminalUserName="
				+ terminalUserName + "]";
	}
}
