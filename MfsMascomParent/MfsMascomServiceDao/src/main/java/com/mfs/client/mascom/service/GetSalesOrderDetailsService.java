package com.mfs.client.mascom.service;

import com.mfs.client.mascom.dto.GetSalesOrderRequestDto;
import com.mfs.client.mascom.dto.GetSalesOrderResponseDto;

public interface GetSalesOrderDetailsService {

	GetSalesOrderResponseDto salesOrderDetails(GetSalesOrderRequestDto requestDto);
	
}
