package com.mfs.client.mascom.service;

import com.mfs.client.mascom.dto.PaymentRequestDto;
import com.mfs.client.mascom.dto.PaymentResponseDto;
import com.mfs.client.mascom.exception.DaoException;

public interface PaymentService {
	
	public PaymentResponseDto paymentDetail(PaymentRequestDto request) ;

}
