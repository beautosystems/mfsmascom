package com.mfs.client.mascom.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
@XmlRootElement(name = "TCSRequest")
@XmlType(propOrder = { "userName", "password", "terminalType","function" })
public class GetSalesOrderRequestDto {

	private String userName;
	private String password;
	private String terminalType;
	private SalesOrder function;

	@XmlElement(name = "UserName")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@XmlElement(name = "Password")
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@XmlElement(name = "TERMINALTYPE")
	public String getTerminalType() {
		return terminalType;
	}

	public void setTerminalType(String terminalType) {
		this.terminalType = terminalType;
	}

	@XmlElement(name = "Function")
	public SalesOrder getFunction() {
		return function;
	}

	public void setFunction(SalesOrder function) {
		this.function = function;
	}

	@Override
	public String toString() {
		return "GetSalesOrderXmlRequestDto [userName=" + userName + ", password=" + password + ", terminalType="
				+ terminalType + ", function=" + function + "]";
	}

}
