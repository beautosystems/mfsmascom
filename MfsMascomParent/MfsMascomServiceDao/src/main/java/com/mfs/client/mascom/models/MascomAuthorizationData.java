package com.mfs.client.mascom.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mascom_authorization_token")
public class MascomAuthorizationData {

	@Id
	@GeneratedValue
	@Column(name = "auth_id")
	private int authId;

	@Column(name = "access_token")
	private String accessToken;

	@Column(name = "date_logged")
	private Date dateLogged;

	public int getAuthId() {
		return authId;
	}

	public void setAuthId(int authId) {
		this.authId = authId;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Date getDateLogged() {
		return dateLogged;
	}

	public void setDateLogged(Date dateLogged) {
		this.dateLogged = dateLogged;
	}

	@Override
	public String toString() {
		return "MascomAuthorizationData [authId=" + authId + ", accessToken=" + accessToken + ", dateLogged="
				+ dateLogged + "]";
	}

}
