package com.mfs.client.mascom.dao.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mascom.dao.TransactionDao;
import com.mfs.client.mascom.exception.DaoException;
import com.mfs.client.mascom.models.MascomSalesOrderTransactionDetails;
import com.mfs.client.mascom.models.MascomTransactionLog;
import com.mfs.client.mascom.models.MascomViewAccount;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class TransactionDaoTest {

	@Autowired
	TransactionDao transactionDao;
	
	@Ignore
	@Test
	public void salesOrderByMfsIdOrSalesOrder() throws DaoException {
		
		MascomSalesOrderTransactionDetails mascomSalesOrderTransactionDetails = transactionDao.getByMfsIdOrSalesOrderId("123456");
		System.out.println(mascomSalesOrderTransactionDetails);
		Assert.assertNotNull(mascomSalesOrderTransactionDetails);
	}
	
	@Ignore
	@Test
	public void salesOrderByMfsIdOrSalesOrderNullTest() throws DaoException {
		
		MascomSalesOrderTransactionDetails mascomSalesOrderTransactionDetails = transactionDao.getByMfsIdOrSalesOrderId("111");
		System.out.println(mascomSalesOrderTransactionDetails);
		Assert.assertNull(mascomSalesOrderTransactionDetails);
	}
	
	@Ignore
	@Test
	public void getDetailByMfsTransactionId() throws DaoException {
		
		MascomTransactionLog transactionDetails = transactionDao.getDetailByMfsTransactionId("1");
		System.out.println(transactionDetails);
		Assert.assertNotNull(transactionDetails);
	}
	
	
	//	@Ignore
		@Test
		public void getByMsisdnOrAccountId() throws DaoException {
			
			MascomViewAccount viewAccountTransactionDetails = transactionDao.getByMsisdnOrAccountId("111", "");
			System.out.println(viewAccountTransactionDetails);
			Assert.assertNotNull(viewAccountTransactionDetails);
		}
}
