package com.mfs.client.mascom.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mascom.dto.GetSalesOrderRequestDto;
import com.mfs.client.mascom.dto.GetSalesOrderResponseDto;
import com.mfs.client.mascom.dto.SalesOrder;
import com.mfs.client.mascom.service.GetSalesOrderDetailsService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetSalesOrderServiceTest {

	@Autowired
	GetSalesOrderDetailsService getSalesOrderDetailsService;

	// Check For Success test.
	@Ignore
	@Test
	public void getSalesOrderServiceTest() throws Exception {

		GetSalesOrderRequestDto requestDto = new GetSalesOrderRequestDto();

		requestDto.setUserName("admin");
		requestDto.setPassword("123456");
		requestDto.setTerminalType("WEB");
		SalesOrder request = new SalesOrder();
		request.setMfsTransactionId("1122");
		requestDto.setFunction(request);

		GetSalesOrderResponseDto responseDto = getSalesOrderDetailsService.salesOrderDetails(requestDto);
		System.out.println(responseDto);
		Assert.assertEquals("Success", responseDto.getSalesOrderStatus());
	}

}
