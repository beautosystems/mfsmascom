package com.mfs.client.mascom.dao.test;

import java.util.Map;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.Assert;

import com.mfs.client.mascom.dao.SystemConfigDao;
import com.mfs.client.mascom.exception.DaoException;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class MascomSystemConfigTest {

	@Autowired
	SystemConfigDao systemConfigDao;

	@Ignore
	@Test
	public void systemConfigTest() throws DaoException  
	{
		Map<String,String> map = systemConfigDao.getConfigDetailsMap();
		System.out.println(map);
		Assert.assertNotNull(map);
	}

}
