package com.mfs.client.mascom.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mascom.dto.AuthorizationResponseDto;
import com.mfs.client.mascom.service.AuthorizationService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class AuthorizationServiceTest {

	@Autowired
	AuthorizationService authorizationService;

	// function to test success
	@Ignore
	@Test
	public void authorizationServiceTest() throws Exception {

		AuthorizationResponseDto response = authorizationService.authorizationService();
		System.out.println(response);
		Assert.assertEquals("69c5-abca-416-8a2f-ac5d76a", response.getAccess_token());
	}

	// function to test not null
	@Test
	@Ignore
	public void authorizationServiceNotNullTest() throws Exception {
		AuthorizationResponseDto response = authorizationService.authorizationService();
		System.out.println(response);
		Assert.assertNotNull(response);

	}
}
