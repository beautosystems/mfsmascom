package com.mfs.client.mascom.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.mascom.dto.ViewAccountType;
import com.mfs.client.mascom.dto.ViewAccountTypeRequestDto;
import com.mfs.client.mascom.service.ViewAccountTypeService;


@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class ViewAccountTypeRestAssureTest {

	@Autowired
	ViewAccountTypeService viewAccountTypeService;

	@Test
	public void viewAccountTypeTest() {
		
		ViewAccountTypeRequestDto request=new ViewAccountTypeRequestDto();
		ViewAccountType dto=new ViewAccountType();
		request.setUserName("admin");
		request.setPassword("123456");
		request.setTerminalType("POS");
		
		dto.setAccountMsisdn("123456789");
		request.setFunction(dto);
		given().body(request).contentType(ContentType.JSON).when()
		.post("http://localhost:8080/MfsMascomWeb/viewAcountType").then().statusCode(equalTo(HttpStatus.OK.value()))
		.body("status", is("A"));
	}
}
