package com.mfs.client.mascom.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.mascom.dto.GetSalesOrderRequestDto;
import com.mfs.client.mascom.dto.SalesOrder;
import com.mfs.client.mascom.service.GetSalesOrderDetailsService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class GetSalesOrderDetailsRestAssureTest {

	@Autowired
	GetSalesOrderDetailsService getSalesOrderDetailsService;

	// Check For Success test.
	@Ignore
	@Test
	public void getSalesOrderTest() {

		GetSalesOrderRequestDto requestDto = new GetSalesOrderRequestDto();

		requestDto.setUserName("admin");
		requestDto.setPassword("123456");
		requestDto.setTerminalType("WEB");
		SalesOrder request = new SalesOrder();
		request.setMfsTransactionId("123456");
		requestDto.setFunction(request);

		given().body(requestDto).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MfsMascomWeb/salesOrder").then().statusCode(equalTo(HttpStatus.OK.value()))
				.body("salesOrderStatus", is("Success"));

	}

	// Check For Validation test.
	@Ignore
	@Test
	public void getSalesOrderValidationTest() {

		GetSalesOrderRequestDto requestDto = new GetSalesOrderRequestDto();

		requestDto.setUserName("admin");
		requestDto.setPassword("123456");
		requestDto.setTerminalType("WEB");
		SalesOrder request = new SalesOrder();
		request.setMfsTransactionId("");
		requestDto.setFunction(request);

		given().body(requestDto).contentType(ContentType.JSON).when()
				.post("http://localhost:8080/MfsMascomWeb/salesOrder").then().statusCode(equalTo(HttpStatus.OK.value()))
				.body("message", is("Validation Error:Mfs Transaction Id Field is required"));

	}
}
