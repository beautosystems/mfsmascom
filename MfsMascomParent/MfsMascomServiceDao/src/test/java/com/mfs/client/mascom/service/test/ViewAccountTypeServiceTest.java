package com.mfs.client.mascom.service.test;

import org.junit.Assert;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mascom.dto.ViewAccountType;
import com.mfs.client.mascom.dto.ViewAccountTypeRequestDto;
import com.mfs.client.mascom.dto.ViewAccountTypeResponseDto;
import com.mfs.client.mascom.service.ViewAccountTypeService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class ViewAccountTypeServiceTest {
	
	
	@Autowired
	ViewAccountTypeService viewAccountTypeService;
	
//	@Ignore
	@Test
	public void viewAccountTypeByMsisdnSuccessTest() {
		
		ViewAccountTypeRequestDto request=new ViewAccountTypeRequestDto();
		ViewAccountType dto=new ViewAccountType();
		request.setUserName("admin");
		request.setPassword("123456");
		request.setTerminalType("POS");
		
		dto.setAccountMsisdn("111");
		request.setFunction(dto);
		ViewAccountTypeResponseDto response=viewAccountTypeService.viewAccountType(request);
		Assert.assertEquals("01", response.getAccountMsisdn());
			
	}	
	

}
