package com.mfs.client.mascom.service.test;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.mfs.client.mascom.dto.Payment;
import com.mfs.client.mascom.dto.PaymentRequestDto;
import com.mfs.client.mascom.dto.PaymentResponseDto;
import com.mfs.client.mascom.service.PaymentService;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class PaymentServiceTest {

	@Autowired
	PaymentService paymentService;

	// for success test

	@Ignore
	@Test
	public void getPaymentServiceTest() throws Exception {

		PaymentRequestDto request = new PaymentRequestDto();
		request.setCheckOnly("false");
		Payment pay = new Payment();
		pay.setMsisdn("1122");
		pay.setRemittanceAmount(12.0);
		pay.setBrandId("89");
		pay.setForexRate("2");
		pay.setSenderAmountCurrency("35");
		pay.setSenderAccountNumber("2435");
		pay.setSenderAmount("57");
		pay.setMfsTransactionId("2");
		pay.setMfsSystemTransactionNumber("565");
		request.setFunction(pay);

		PaymentResponseDto responseDto = paymentService.paymentDetail(request);
		System.out.println(responseDto);
		Assert.assertEquals("0", responseDto.getCode());
	}

	// for fail test

		//@Ignore
		@Test
		public void getPaymentServiceFailTest() throws Exception {

			PaymentRequestDto request = new PaymentRequestDto();
			request.setCheckOnly("false");
			Payment pay = new Payment();
			pay.setMsisdn("1122");
			pay.setRemittanceAmount(12.0);
			pay.setBrandId("89");
			pay.setForexRate("2");
			pay.setSenderAmountCurrency("35");
			pay.setSenderAccountNumber("2435");
			pay.setSenderAmount("57");
			pay.setMfsTransactionId("2");
			pay.setMfsSystemTransactionNumber("565");
			request.setFunction(pay);

			PaymentResponseDto responseDto = paymentService.paymentDetail(request);
			System.out.println(responseDto);
			Assert.assertEquals("ER216", responseDto.getCode());
		}

	
}
