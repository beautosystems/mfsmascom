package com.mfs.client.mascom.restassure.test;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.jayway.restassured.http.ContentType;
import com.mfs.client.mascom.dto.Payment;
import com.mfs.client.mascom.dto.PaymentRequestDto;

@ContextConfiguration(locations = "classpath:applicationServiceDaoContext.xml")
@RunWith(SpringRunner.class)
public class PaymentServiceRestAssureTest {
	//@Ignore
	@Test
	public void getPaymentServiceTest() throws Exception {

		PaymentRequestDto request = new PaymentRequestDto();
		request.setCheckOnly("false");
		Payment pay = new Payment();
		pay.setMsisdn("1122");
		pay.setRemittanceAmount(12.0);
		pay.setBrandId("89");
		pay.setForexRate("2");
		pay.setSenderAmountCurrency("35");
		pay.setSenderAccountNumber("2435");
		pay.setSenderAmount("57");
		pay.setMfsTransactionId("23");
		pay.setMfsSystemTransactionNumber("565");
		request.setFunction(pay);

		given().body(request).contentType(ContentType.JSON).when()
		.post("http://localhost:8080/MfsMascomWeb/payment").then().statusCode(equalTo(HttpStatus.OK.value()))
		.body("code", is("0"));

	}

}
