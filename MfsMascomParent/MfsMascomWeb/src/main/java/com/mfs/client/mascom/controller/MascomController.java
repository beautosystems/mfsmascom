package com.mfs.client.mascom.controller;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.mfs.client.mascom.dto.AuthorizationResponseDto;
import com.mfs.client.mascom.dto.GetSalesOrderRequestDto;
import com.mfs.client.mascom.dto.GetSalesOrderResponseDto;
import com.mfs.client.mascom.dto.PaymentRequestDto;
import com.mfs.client.mascom.dto.PaymentResponseDto;
import com.mfs.client.mascom.dto.SalesOrder;
import com.mfs.client.mascom.dto.ViewAccountType;
import com.mfs.client.mascom.dto.ViewAccountTypeRequestDto;
import com.mfs.client.mascom.dto.ViewAccountTypeResponseDto;
import com.mfs.client.mascom.service.AuthorizationService;
import com.mfs.client.mascom.service.GetSalesOrderDetailsService;
import com.mfs.client.mascom.service.PaymentService;
import com.mfs.client.mascom.service.ViewAccountTypeService;
import com.mfs.client.mascom.util.CommonConstant;
import com.mfs.client.mascom.util.CommonValidations;
import com.mfs.client.mascom.util.MascomCodes;

@RestController
public class MascomController {

	@Autowired
	ViewAccountTypeService viewAccountTypeService;

	@Autowired
	PaymentService paymentService;

	@Autowired
	AuthorizationService authService;

	@Autowired
	GetSalesOrderDetailsService getSalesOrderDetailsService;

	// function to generate Access Token
	@GetMapping(value = "/auth")
	public AuthorizationResponseDto authenticate() {
		AuthorizationResponseDto response = authService.authorizationService();
		return response;
	}

	// function to get sales order details
	@PostMapping(value = "/salesOrder", produces = "application/json")
	public GetSalesOrderResponseDto salesOrder(@RequestBody GetSalesOrderRequestDto requestDto) {

		GetSalesOrderResponseDto response = new GetSalesOrderResponseDto();
		SalesOrder function = requestDto.getFunction();
		CommonValidations validation = new CommonValidations();

		if (validation.validateStringValues(function.getMfsTransactionId())) {
			response.setCode(MascomCodes.VALIDATION_ERROR.getCode());
			response.setMessage(MascomCodes.VALIDATION_ERROR.getMessage() + ":" + CommonConstant.INVALID_MFSTRANS_ID);

		} else {
			response = getSalesOrderDetailsService.salesOrderDetails(requestDto);
		}
		return response;
	}

	// This method for save and update wallet transaction payment Detail
	@RequestMapping(value = "/payment", method = RequestMethod.POST, produces = "application/json")
	public PaymentResponseDto paymentServise(@RequestBody PaymentRequestDto request) {
		PaymentResponseDto response = new PaymentResponseDto();
		CommonValidations commonValidations = new CommonValidations();
		if (commonValidations.validateNumber(request.getFunction().getMsisdn())) {
			response.setCode(MascomCodes.REQUIRED_MSISDN.getCode());
			response.setMessage(MascomCodes.REQUIRED_MSISDN.getMessage());
		} else if (commonValidations.validateAmountValues(request.getFunction().getRemittanceAmount())) {
			response.setCode(MascomCodes.REQUIRED_REMITTANCE_AMOUNT.getCode());
			response.setMessage(MascomCodes.REQUIRED_REMITTANCE_AMOUNT.getMessage());
		} else if (commonValidations.validateStringValues(request.getFunction().getBrandId())) {
			response.setCode(MascomCodes.REQUIRED_BRAND_ID.getCode());
			response.setMessage(MascomCodes.REQUIRED_BRAND_ID.getMessage());
		} else if (commonValidations.validateStringValues(request.getFunction().getMfsTransactionId())) {
			response.setCode(MascomCodes.REQUIRED_MFS_TRANSACTION_ID.getCode());
			response.setMessage(MascomCodes.REQUIRED_MFS_TRANSACTION_ID.getMessage());
		} else {
			response = paymentService.paymentDetail(request);
		}
		return response;
	}

	// function for view Account Type Service
		@RequestMapping(value = "/viewAcountType", method = RequestMethod.POST, produces = "application/json")
		public ViewAccountTypeResponseDto viewAccountType(@RequestBody ViewAccountTypeRequestDto request) {

			ViewAccountTypeResponseDto response = new ViewAccountTypeResponseDto();
			ViewAccountType function = request.getFunction();

			CommonValidations validation = new CommonValidations();

			if (validation.validateStringValues(function.getAccountMsisdn())
					&& validation.validateStringValues(function.getAccountId())) {
				response.setCode(MascomCodes.BOTH_ID_CANNOT_BE_NULL.getCode());
				response.setMessage(MascomCodes.BOTH_ID_CANNOT_BE_NULL.getMessage());
			} else if (validation.validateNumber(function.getAccountMsisdn())
					&& validation.validateNumber(function.getAccountId())) {

				response.setCode(MascomCodes.INVALID_MSISDN_OR_ACCOUNT_ID.getCode());
				response.setMessage(MascomCodes.INVALID_MSISDN_OR_ACCOUNT_ID.getMessage());
				
			} else {

				response = viewAccountTypeService.viewAccountType(request);
			}

			return response;
		}
}
